[[$jQMobile Page begin?]]
	<div data-role="page" id="sessions">
		<div data-role="header" class="heading">
			<h1>Session</h1>
		</div>
		<div data-role="content" class="content">
			[[*Page Intro]]

			[[!Wayfinder?
				&startId=`8`
				&debug=`0`
				&outerTpl=`Session List Outer`
				&rowTpl=`Session List Row`
				&scheme=`abs`]]
		</div>
		[[$Footer-Back-Schedule-Favorites?]]
	</div>
[[$jQMobile Page end?]]