<?php
/* first, build the query */
$c = $modx->newQuery('modResource');

/* we only want published and undeleted resources */
$c->innerJoin('modTemplateVarResource','TemplateVarResources');
$c->innerJoin('modTemplateVar','TemplateVar','`TemplateVar`.`id` = `TemplateVarResources`.`tmplvarid` AND `TemplateVar`.`name` = "Session Start Date"');
$c->where(array(
  'published' => true,
  'deleted' => false,
));

/* get all the children of ID 390 */
$children = $modx->getChildIds(8);
if (count($children) > 0) {
    $c->where(array(
        'id:IN' => $children,
    ));
}

/* sort by menuindex ascending */
$c->sortby('`TemplateVarResources`.`value`','ASC');

/* get the resources as xPDOObjects */
$resources = $modx->getCollection('modResource',$c);

$output = '';
$oldStartDate = '';
$newStartTime = '';

	// Check for Day variable, if not, list Day buttons
	if( isset($_GET['day']) ){
		
		foreach ($resources as $resource) {
			$phpDate = strtotime($resource->getTVValue('Session Start Date'));
			
			if( date('FjY', $phpDate) == $_GET['day'] ){
				
				// Print the Date Header
				$newStartDate = date('mdY', $phpDate);
				if($newStartDate != $oldStartDate){
					$oldStartDate = $newStartDate;
					$output .= '<h3>'.date('l F j, Y', $phpDate).'</h3>';
				}
				
				// Print the Time Separator
				$newStartTime = date('g:i a', $phpDate);
				if($newStartTime != $oldStartTime){
					$oldStartTime = $newStartTime;
					$output .= '<h5>'.date('g:i a', $phpDate).'</h5>';
				}
				
				// Print the session button
				$output .= $modx->getChunk('Session Button',array(
					'pagetitle' => $resource->get('pagetitle'),
					'id' => $resource->get('id'),
					'tv.Session Start Date' => $resource->getTVValue('Session Start Date'),
					'tv.Session End Date' => $resource->getTVValue('Session End Date'),
					'tv.Session Speakers' => $resource->getTVValue('Session Speakers')
				));
			}
				
		}
	}else{
		foreach ($resources as $resource) {
			$phpDate = strtotime($resource->getTVValue('Session Start Date'));
			$newStartDate = date('mdY', $phpDate);
			if($newStartDate != $oldStartDate){
				$oldStartDate = $newStartDate;
				$output .= '<a class="agenda_button_day" data-prefetch="true" data-role="button" data-icon="arrow-r" data-iconpos="right" href="/agenda?day='.date('FjY', $phpDate).'"><span class="agenda_date"">'.date('M j', $phpDate).'</span>'.date('l', $phpDate).'</a>';
			}
		}	
	}


print $output;