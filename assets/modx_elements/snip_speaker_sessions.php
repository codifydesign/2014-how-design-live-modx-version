<?php
/* first, build the query */
$c = $modx->newQuery('modResource');
/* we only want published and undeleted resources */

$c->innerJoin('modTemplateVarResource','TemplateVarResources');
$c->innerJoin('modTemplateVar','TemplateVar','`TemplateVar`.`id` = `TemplateVarResources`.`tmplvarid` AND `TemplateVar`.`name` = "Session Start Date"');
$c->where(array(
  'published' => true,
  'deleted' => false,
));
/* get all the children of ID 390 */
$children = $modx->getChildIds(8);
if (count($children) > 0) {
    $c->where(array(
        'id:IN' => $children,
    ));
}
/* sort by menuindex ascending */
$c->sortby('`TemplateVarResources`.`value`','ASC');
/* get the resources as xPDOObjects */
$resources = $modx->getCollection('modResource',$c);

$output = '';
$this_speaker = $modx->resource->get('id');  // gets the ID of the resource calling this snippet
$session_speaker = '';
$count = 0;

foreach ($resources as $resource) {
	$session_speaker = $resource->getTVValue('Session Speakers');
	
	$session_speaker_arr = explode(",", $session_speaker);
		
	if( in_array($this_speaker, $session_speaker_arr) ){

		$count += 1;
		$output .= $modx->getChunk('Session Button',array(
			'pagetitle' => $resource->get('pagetitle'),
			'id' => $resource->get('id'),
			'tv.Session Start Date' => $resource->getTVValue('Session Start Date'),
			'tv.Session End Date' => $resource->getTVValue('Session End Date'),
			'tv.Session Speakers' => $resource->getTVValue('Session Speakers')
		));
	}
}

$this_speaker_first_name = $modx->resource->getTVValue('Speaker First Name');
if ( $count > 1 ){ $plural = 's'; }else{ $plural = ''; }

print '<h4 class="speaker_sessions">'.$this_speaker_first_name.'\'s Session'.$plural.'</h4>';

print $output;