<?php
/* first, build the query */
$c = $modx->newQuery('modResource');
/* we only want published and undeleted resources */

$c->innerJoin('modTemplateVarResource','TemplateVarResources');
$c->innerJoin('modTemplateVar','TemplateVar','`TemplateVar`.`id` = `TemplateVarResources`.`tmplvarid` AND `TemplateVar`.`name` = "Session Start Date"');

$c->where(array(
  'published' => true,
  'deleted' => false,
));
/* get all the children of ID 390 */
$children = $modx->getChildIds(8);
if (count($children) > 0) {
    $c->where(array(
        'id:IN' => $children,
    ));
}
/* sort by menuindex ascending */
$c->sortby('`TemplateVarResources`.`value`','ASC');
/* get the resources as xPDOObjects */
$resources = $modx->getCollection('modResource',$c);

$oldStartDate = '';
$output = '';
foreach ($resources as $resource) {
	$phpDate = strtotime($resource->getTVValue('Session Start Date'));
	$newStartDate = date('mdY', $phpDate);
	$newStartTime = date('g:i a', $phpDate);
	
	if($newStartDate != $oldStartDate){
		$oldStartDate = $newStartDate;
		$output .= '<h3>'.date('l F j, Y', $phpDate).'</h3>';
		
	}else{
		
		
	}
	
	$output .= $modx->getChunk('Session Button',array(
		'pagetitle' => $resource->get('pagetitle'),
		'id' => $resource->get('id'),
		'tv.Session Start Date' => date('g:i a', $phpDate),
		'tv.Session Speaker' => $resource->getTVValue('Session Speaker'),
		
	));

}
return $output;