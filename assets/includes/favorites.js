var favoriteArray = new Array();


$(document).ready(function () {

	if(localStorage.getItem('sessionFavorites') != null && localStorage.getItem('sessionFavorites') != '') {

		favoriteArray = JSON.parse(localStorage.getItem('sessionFavorites'));
		loadFavorites();
		setFavoriteButtons();
	};

	$('.selectable_item .favorite').on('click',function(){

		if($(this).hasClass('success')){
			
			addFavorite($(this).closest('.selectable_item').attr('data-id'));				
		}else{
			removeFavorite($(this).closest('.selectable_item').attr('data-id'));	
		}
	});
		
	$('.fav_list_item .remove').on('click',function(){
		removeFavorite($(this).closest('.fav_list_item').attr('data-id'));
	});

});
			
			
			
function loadFavorites(){
	
	$('.fav_list_container').html();
	var favItemsList = "";
	for (var i = 0; i < favoriteArray.length; i++){
		favItemsList += '<div class="fav_list_item" data-id="'+favoriteArray[i]['id']+'"><a class="session" data-role="button" data-icon="arrow-r" data-iconpos="right" href="#session_'+favoriteArray[i]['id']+'">'+favoriteArray[i]['title']+'<br/><span style="font-weight:normal;">'+favoriteArray[i]['date']+'</span><br/><span style="font-weight:normal;font-style:italic;">'+favoriteArray[i]['speaker']+'</span></a><a class="remove" href="javascript:">remove</a><div class="clear_fix"></div></div>';
	}
	$('.fav_list_container').html(favItemsList);
	$('.fav_list_container a.session').buttonMarkup();  //Adds jQuery Mobile button markup
	if( $('.fav_list_container').html() == '' ){
		$('.fav_list_container').html('<p>You have no favorites selected.</p>');
	}
}

function setFavoriteButtons(){

	$('.selectable_item').each(function(){
		if(findFavorate($(this).attr('data-id')) > -1){
			$(this).find('.favorite').removeClass('success').addClass('alert').html('Remove From Favorites');	
		}else{
			$(this).find('.favorite').removeClass('alert').addClass('success').html('Add To Favorites');	
		}
	});
	
	$('a.session').each(function(){
		if(findFavorate($(this).attr('data-session-id')) > -1){
			$(this).addClass('favorite');
		}else{
			$(this).removeClass('favorite');
		}
	});

}

function findFavorate(id){
	for (var i = 0; i < favoriteArray.length; i++){
		if(favoriteArray[i]['id'] == id) return i;
	}
	return -1;
}

function addFavorite(id){

	if(findFavorate(id) > -1) return;	
	favoriteArray.push({'id':id,'title':$('.selectable_item[data-id="'+id+'"]').attr('data-session-title'),'date':$('.selectable_item[data-id="'+id+'"]').attr('data-session-date'),'speaker':$('.selectable_item[data-id="'+id+'"]').attr('data-session-speaker')});
	localStorage.setItem("sessionFavorites",JSON.stringify(favoriteArray));
	loadFavorites();
	setFavoriteButtons();	
}

function removeFavorite(id){
	if(findFavorate(id) == -1) return;
	favoriteArray.splice(findFavorate(id),1);	
	localStorage.setItem("sessionFavorites",JSON.stringify(favoriteArray));
	loadFavorites();
	setFavoriteButtons();	
}


